from time import perf_counter


def silnia(n: int) -> int:
    if n <= 1:
        return 1
    else:
        return n * silnia(n-1)


def newton(n: int, k: int) -> int:
    global resultList
    if n == k or k == 0:
        return 1
    else:
        return resultList[n-1][k-1] + resultList[n-1][k]


def createResultList(n, k):
    global resultList

    resultList = [0] * (n+1)

    i = 1
    while i <= n + 1:
        resultList[i] = [0]*(k+1)
        i += 1

    i = 1
    while i <= n+1:
        resultList[i] = [0]*k
        j = 1
        while j <= min(n, k+1):
            resultList[i][j] = newton(i, j)
            j += 1
        i += 1


while True:
    n = input('Podaj n (x aby zakończyć): ')
    k = input('Podaj k (x aby zakończyć): ')

    if n == 'x' or k == 'x':
        exit(0)

    n = int(n)
    k = int(k)

    if 0 <= k <= n:
        t1_start = perf_counter()
        resultList = []
        createResultList(n, k)
        print('n = ' + str(n) + ', k = ' + str(k) + ', wynik = ' + str(newton(n, k)))
        t1_stop = perf_counter()
        print("Elapsed time during the whole program in seconds:",
              t1_stop - t1_start)
    else:
        print('Niepoprawne dane wejściowe')
