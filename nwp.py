def findNWP(str1, str2):
    str1Len = len(str1)
    str2Len = len(str2)

    C = [0] * (str1Len + 1)

    for i in range(0, str1Len + 1):
        C[i] = [0] * (str2Len + 1)

    for i in range(1, str1Len + 1):
        for j in range(1, str2Len + 1):
            if str1[i - 1] == str2[j - 1]:
                C[i][j] = C[i - 1][j - 1] + 1
            else:
                C[i][j] = max(C[i - 1][j], C[i][j - 1])
    return C


def getNWP(str1, str2):
    C = findNWP(str1, str2)
    result = ''
    i = len(str1)
    j = len(str2)

    while i >= 0 and j >= 0:
        if i == 0 and j == 0:
            return result
        elif i > 0 and C[i][j] == C[i - 1][j]:
            i -= 1
            continue
        elif j > 0 and C[i][j] == C[i][j - 1]:
            j -= 1
            continue
        else:
            result = str1[i-1] + result
            j -= 1
            i -= 1


str1 = input('Podaj pierwszy ciąg: ')
str2 = input('Podaj drugi ciąg: ')

nwp = getNWP(str1, str2)
print('Najdłuszy wspólny podciąg: ', nwp)
print('długość: ', len(nwp))
