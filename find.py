import random
import string
import matplotlib.pyplot as plt
CMP_COUNT = 0

def matchesAt(t: str, pos: int, w: str) -> bool:
    global CMP_COUNT

    for i in range(len(w)):
        CMP_COUNT += 1
        if w[i] != t[pos + i]:
            return False

    return True

def naive(t: str, w: str) -> list:
    result = []

    for i in range(len(t) - len(w) + 1):
        if matchesAt(t, i, w):
            result.append(i)

    return result

def sunday(t: str, w: str) -> list:
    result = []
    occ = {}

    for i in range(0, len(t)):
        occ[t[i]] = -1

    for j in range(0, len(w)):
        occ[w[j]] = j

    i = 0

    while i <= len(t) - len(w):
        if matchesAt(t, i, w):
            result.append(i)
        i += len(w)
        if i < len(t):
            i -= occ[t[i]]

    return result


def TransformPattern_KMP(pattern):
    KMPNext_size = len(pattern) + 1
    KMPNext = [0] * KMPNext_size
    pos = KMPNext[0] = -1

    for i in range(1, KMPNext_size):
        while pos > -1 and pattern[pos] != pattern[i - 1]:
            pos = KMPNext[pos]
        pos += 1

        if i == KMPNext_size-1 or pattern[i] != pattern[pos]:
            KMPNext[i] = pos
        else:
            KMPNext[i] = KMPNext[pos]

    return KMPNext

def kmp(s,pattern):
    global CMP_COUNT
    KMPNext = TransformPattern_KMP(pattern)
    b = 0
    result = []
    for i, char_S in enumerate(s):
        while b > -1 and pattern[b] != char_S:
            CMP_COUNT += 1
            b = KMPNext[b]
        b += 1
        if b == len(pattern):
            CMP_COUNT += 1
            result.append(i-len(pattern)+1)
            b = KMPNext[b] - 1
    return result


def benchmark(alg, t, w):
    global CMP_COUNT
    CMP_COUNT = 0
    alg(t, w)
    return CMP_COUNT

def getRandStr(size: int, alphabetSize: int) -> str:
    result = ''

    for _ in range(size):
        result += random.choice(string.ascii_letters[:alphabetSize])

    return result

def test1(alg):
    result = {
        'x': [],
        'y': [],
    }

    w = getRandStr(10, 10)
    t = getRandStr(10, 10)
    for i in range(10, 200):
        result['x'].append(i)
        result['y'].append(benchmark(alg, t, w))

        t = t + getRandStr(1, 10)

    return result


def test2(alg):
    result = {
        'x': [],
        'y': [],
    }

    w = getRandStr(10, 10)
    t = getRandStr(200, 10)
    for i in range(10, 100):
        result['x'].append(i)
        result['y'].append(benchmark(alg, t, w))

        w = w + getRandStr(1, 10)

    return result


def test3(alg):
    result = {
        'x': [],
        'y': [],
    }

    for i in range(1, 20):
        w = getRandStr(20, i)
        t = getRandStr(100, i)
        result['x'].append(i)
        result['y'].append(benchmark(alg, t, w))

    return result


fig, (t1, t2, t3) = plt.subplots(1, 3)
t_naive = test1(naive)
t_sunday = test1(sunday)
t_kmp = test1(kmp)
t1.plot(t_naive['x'], t_naive['y'], label='Naive')
t1.plot(t_sunday['x'], t_sunday['y'], label='Sunday')
t1.plot(t_kmp['x'], t_kmp['y'], label='KMP')
t1.set_xlabel('długość tekstu')
t1.set_ylabel('liczba porównań')
t1.legend()

t_naive = test2(naive)
t_sunday = test2(sunday)
t_kmp = test2(kmp)
t2.plot(t_naive['x'], t_naive['y'], label='Naive')
t2.plot(t_sunday['x'], t_sunday['y'], label='Sunday')
t2.plot(t_kmp['x'], t_kmp['y'], label='KMP')
t2.set_xlabel('długość wzorca')
t2.set_ylabel('liczba porównań')
t2.legend()

t_naive = test3(naive)
t_sunday = test3(sunday)
t_kmp = test3(kmp)
t3.plot(t_naive['x'], t_naive['y'], label='Naive')
t3.plot(t_sunday['x'], t_sunday['y'], label='Sunday')
t3.plot(t_kmp['x'], t_kmp['y'], label='KMP')
t3.set_title('W zależności od długości alfabetu')
t3.set_xlabel('długość alfabetu')
t3.set_ylabel('liczba porównań')
t3.legend()

plt.show()