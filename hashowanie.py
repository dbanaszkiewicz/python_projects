import matplotlib.pyplot as plt
import random
from collections.abc import Iterable


class Dictionary:
    def __init__(self, length, value):
        self._tab = []
        
        for i in range(length):
            self.addNewElement(value)

    def setValue(self, key, value):
        self._tab[key] = value

    def getValue(self, key):
        return self._tab[key]

    def hasValue(self, key):
        return length(self._tab[key]) > 0

    def eraseKeyValue(self, key, value):
        self._tab[key].pop(self._tab[key].index(value))

    def getAllValues(self, arrayValues = True):
        all = []
        
        for v in self._tab:
            if arrayValues:
                all += v
            else:
                if not (v is EmptyField or v is EarseField):
                    all.append(v)
        
        return all

    def getLength(self):
        return len(self._tab)

    def addNewElement(self, value):
        self._tab.append(value)

    def getCountOfValues(self):
        return length(self.getAllValues())

    def write(self):
        for i, v in enumerate(self._tab):
            if isinstance(v, Iterable):
                print(str(i) + ":", *v)
            else:
                print(str(i) + ":", v)

def average(lst): 
    return sum(lst) / len(lst) 

class HashTableChain:

    def __init__(self, size):
        self.count = 0
        self.data = Dictionary(size, [])

    def find(self, data, getCount = False):
        IDSlot = self._hashFunction_(data)
        count = 0

        for value in self.data.getValue(IDSlot):
            if value == data:
                count += 1
                if not getCount:
                    return IDSlot
            else:
                count += 1
        
        if getCount:
            return count
        else:
            return -1

    def put(self, data):
        if self.count > 3 * self._len_():
            self.rehash()
        if self.find(data) < 0:
            IDSlot = self._hashFunction_(data)
            self.count += 1
            self.data.setValue(IDSlot, self.data.getValue(IDSlot) + [data])
            return True
        return False

    def erase(self, *args):
        for data in args:
            IDSlot = self._hashFunction_(data)
            if self.find(data) >= 0:
                self.count -= 1
                self.data.eraseKeyValue(IDSlot, data)

    def _len_(self):
        return self.data.getLength()

    def _hashFunction_(self, data):
        return hash(data) % self._len_()

    def rehash(self):
        elements = self.data.getAllValues()

        for val in elements:
            self.erase(val)
        for _ in range(self._len_()):
            self.data.addNewElement([])
        for val in elements:
            self.put(val)

    def write(self):
        self.data.write()

    def getAverageComparationAmount(self):
        countArray = []

        for data in self.data.getAllValues():
            countArray.append(self.find(data, True))

        return average(countArray)


class EmptyField: pass


class EarseField: pass


class HashTableOpen:

    def __init__(self, size):
        self.count = 0
        self.data = Dictionary(size, EmptyField)

    def find(self, data, increase = 0, getCount = False, count = 0):
        IDSlot = self._hashFunction_(data)
        IDSlot = self._hashFunction_(IDSlot + increase)

        if increase == self._len_():
            if getCount:
                count += 1
                return count
            else:
                return -1

        if self.data.getValue(IDSlot) == data:
            if getCount:
                count += 1
                return count
            else:
                return IDSlot
        elif self.data.getValue(IDSlot) is EmptyField:
            if getCount:
                count += 1
                return count
            else:
                return -1
        else:
            if getCount:
                count += 1
                return self.find(data, increase + 1, True, count)
            else:
                return self.find(data, increase + 1)

    def put(self, data):
        if data is EarseField or data is EmptyField:
            return False

        if self.find(data) >= 0:
            return False

        if self.count >= 0.75 * self._len_():
            self.rehash()

        IDSlot = self._hashFunction_(data)
        while not (self.data.getValue(IDSlot) is EmptyField or self.data.getValue(IDSlot) is EarseField):
            IDSlot = self._hashFunction_(IDSlot + 1)

        self.count += 1
        self.data.setValue(IDSlot, data)
        return True

    def erase(self, *args):
        for data in args:
            IDSlot = self.find(data)

            if IDSlot >= 0:
                self.count -= 1
                self.data.setValue(IDSlot, EarseField)

    def _len_(self):
        return self.data.getLength()

    def _hashFunction_(self, data):
        return hash(data) % self._len_()

    def rehash(self):
        elements = self.data.getAllValues(False)
        
        for val in elements:
            self.erase(val)
        for _ in range(self._len_()):
            self.data.addNewElement(EmptyField)
        for val in elements:
            self.put(val)

    def write(self):
        self.data.write()

    def getAverageComparationAmount(self):
        countArray = []

        for data in self.data.getAllValues(False):
            countArray.append(self.find(data, 0, True, 0))

        return average(countArray)


def draw():
    x_size = 500
    max_value = 5000

    y_HT_Chain = []
    y_HT_Open = [] 

    x = []

    T1 = HashTableChain(25)
    T2 = HashTableOpen(25)
    
    for i in range(x_size):
        x.append(i)

        randomInt = random.randint(0,max_value)
        T1_added = T1.put(randomInt)

        while T1_added is False:
            randomInt = random.randint(0,max_value)
            T1_added = T1.put(randomInt)

        T2.put(randomInt)

        y_HT_Chain.append(T1.getAverageComparationAmount())
        y_HT_Open.append(T2.getAverageComparationAmount())
        


    plt.plot(x, y_HT_Chain, label='HT chain')
    plt.plot(x, y_HT_Open, label='HT open')
    plt.xlabel('liczba elementów')
    plt.ylabel('średnia liczba porównań')
    plt.legend()
    plt.show()


draw()
