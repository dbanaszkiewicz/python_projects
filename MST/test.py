#!/usr/bin/python
"""
Modul testujacy oraz ilustrujacy operacje przeprowadzane za pomoca algorytmu Kruskala.
Tworzy graf na podstawie podanych krawedzi i znajduje dla niego minimalne drzewo rozpinajace
"""
import unittest
import graph
import edge
import MSTKruskal
import printGraph as pg
import random


if __name__ == '__main__':
    """
    Trzy grafy, ktore obrazuja wierzcholki, liste sasiedztwa oraz minimalne drzewo rozpinajace
    """
    graph = graph.Graph()
    listaSasiedztwa = []

    for i in range(0, random.randint(5, 20)):
        source = random.randint(0, 9)
        target = random.randint(0, 9)

        while(target == source):
            target = random.randint(0, 9)

        listaSasiedztwa.append(edge.Edge(source, target, random.randint(1, 15)))

    print("\nGraf:")
    for item in listaSasiedztwa:
        graph.add_edge(item)

    print("Lista wierzcholkow:")
    print(graph.list_nodes())

    graph.show_graph()

    kruskal = MSTKruskal.Kruskal(graph)

    pg.PrintGraph(graph, kruskal.make_mst())