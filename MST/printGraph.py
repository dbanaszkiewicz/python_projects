#!/usr/bin/python
"""
Modul zawiera klase definiujaca strukture grafu
"""

from operator import itemgetter
import edge
from graphviz import Digraph


class PrintGraph:
    def __init__(self, graph, mst_edges):
        self.graph = graph
        self.mst_edges = mst_edges

        self.render()

    def isInMst(self, test_edge):
        """
        Funkcja sprawdza, czy krawedz nalezy do minimalnego drzewa rozpinajacego
        """
        if self.mst_edges:
            for edge in self.mst_edges:
                if edge.source == test_edge.source and edge.target == test_edge.target:
                    return True
        return False

    def render(self):

        dot = Digraph(comment='The Round Table')

        for node in self.graph.list_nodes():
            dot.node(str(node), str(node))

        for edge in self.graph.list_edges():
            if self.isInMst(edge):
                dot.edge(str(edge.source), str(edge.target), label=str(edge.weight), dir='none', color='red')
            else:
                dot.edge(str(edge.source), str(edge.target), label=str(edge.weight), dir='none')

        dot.render('test-output/graph', view=True)
