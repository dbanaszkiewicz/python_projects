import os
import sys


def generateLcsArray(str1, str2):
    str1Len = len(str1)
    str2Len = len(str2)

    C = [0] * (str1Len + 1)

    for i in range(0, str1Len + 1):
        C[i] = [0] * (str2Len + 1)

    for i in range(1, str1Len + 1):
        for j in range(1, str2Len + 1):
            if str1[i - 1] == str2[j - 1]:
                C[i][j] = C[i - 1][j - 1] + 1
            else:
                C[i][j] = max(C[i - 1][j], C[i][j - 1])
    return C


def writeLineDiff(str1, str2):
    C = generateLcsArray(str1, str2)
    result = '\r'
    i = len(str1)
    j = len(str2)

    while i >= 0 and j >= 0:
        if i == 0 and j == 0:
            print(result)
            return
        elif i > 0 and C[i][j] == C[i - 1][j]:
            result = colorString(str1[i-1], 'red') + result
            i -= 1
            continue
        elif j > 0 and C[i][j] == C[i][j - 1]:
            result = colorString(str2[j-1], 'green') + result
            j -= 1
            continue
        else:
            result = str1[i-1] + result
            j -= 1
            i -= 1


def colorString(string, color):
    if color == 'red':
        return '\033[91m' + string + '\033[0m'
    elif color == 'green':
        return '\033[93m' + string + '\033[0m'
    return string


def printError(message):
    print(colorString(message, 'red'))


if len(sys.argv) < 3:
    printError('Missing program arguments.')
    print('Valid command syntax: python nwp_diff.py [file1] [file2]')
else:
    file1 = sys.argv[1]
    file2 = sys.argv[2]

    if not os.access(file1, os.R_OK):
        printError('File: ' + file1 + ' is not readable')
    elif not os.access(file2, os.R_OK):
        printError('File: ' + file2 + ' is not readable')
    else:
        file1_handler = open(file1, 'r')
        file2_handler = open(file2, 'r')

        lines_file1 = file1_handler.read()
        lines_file2 = file2_handler.read()

        writeLineDiff(lines_file1.rstrip(), lines_file2.rstrip())
