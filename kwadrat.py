
class Kwadrat:
    def __init__(self, bok=10):
        self.bok = bok

    @property
    def pole(self):
        return self.bok * self.bok

    @property
    def obwod(self):
        return 4 * self.bok


kw = Kwadrat(15)
print(kw.pole)
print(kw.obwod)

